require 'tinybucket'
require 'json'

max_approve_count = ENV['REQUIRED_APPROVES_COUNT']
oauth_token = ENV['BITBUCKET_OAUTH_TOKEN']
oauth_secret = ENV['BITBUCKET_OAUTH_SECRET']
acc_login = ENV['BITBUCKET_ACC_LOGIN']
repo_shrug = ENV['BITBUCKET_REPO_SHRUG']

Tinybucket.configure do |config|

  config.oauth_token = oauth_token
  config.oauth_secret = oauth_secret
end

bucket = Tinybucket.new

repos = bucket.repos('ichina93')

repo = bucket.repo(acc_login, repo_shrug)

pull_requests = repo.pull_requests(state: 'OPEN')
puts pull_requests
pull_requests.each do |pr|
  
  com_arr = pr.commits
  commits = com_arr.take(com_arr.count)
  commit = commits.first
  puts "commit #{commit}"
  puts "pr #{pr.title}; last commit - #{commit.message}"

  status_arr = commit.build_statuses
  statuses = status_arr.take(status_arr.count)
  puts "statuses #{statuses}"
  failed = statuses.find { |status| status.state != "SUCCESSFUL" }

  if failed == nil && statuses.count
    puts "all success statuses"

    full_pr = repo.pull_request(pr.id)

    participants = full_pr.participants

    count = participants.count { |participant| participant['approved'] }

    if count >= Integer(max_approve_count)
      full_pr.merge 
      puts "merged"
    end
  end
end

puts "end pr1";
